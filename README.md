# Sir M. Visvesvaraya Institute of Technology's Logo
[![PSVersion](https://img.shields.io/badge/Photoshop%20Version-CC%202017-blue.svg)](https://en.wikipedia.org/wiki/Adobe_Photoshop#CC_2017) [![License](https://img.shields.io/badge/license-MIT-red.svg)](LICENSE)

High Resolution PNG files for the Logo of Sir M. Visvesvaraya Institute of Technology in Black and White colors.

## Logo Sample (BLACK) :
![Black Logo](MVIT LOGO \(black\).png )
